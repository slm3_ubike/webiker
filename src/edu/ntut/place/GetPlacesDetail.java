package edu.ntut.place;

import org.json.JSONException;
import org.json.JSONObject;

import edu.ntut.ubike.MainActivity;
import edu.ntut.ubike.R;
import android.os.AsyncTask;

public class GetPlacesDetail extends AsyncTask<Void, Void, Place> {
	private  MainActivity _activity;
	private int _type;
	private Place _place;
	
	public GetPlacesDetail(Place place, MainActivity activity,int type) {
		_place = place;
		_activity = activity;
		_type = type;
	}

	@Override
	protected void onPostExecute(Place result) {
		super.onPostExecute(result);
		//activity show
		switch(_type)
		{
			case R.string.EAT:
				_activity.addEatLocation(result);
				break;
			
			case R.string.BUS:
				_activity.addBusLocation(result);
				break;
			
			case R.string.SEVEN:
				_activity.addSevenLocation(result);
				break;
				
			case R.string.ART:
				_activity.addArtLocation(result);
				break;
				
			case R.string.MRT:
				_activity.addMRTLocation(result);
				break;
		}
		
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected Place doInBackground(Void... arg0) {
		PlaceService service = new PlaceService(_activity.getString(R.string.BROWSER_KEY));
		
		JSONObject detail;
		try {
			detail = service.findPlaceDetail(_place.getReference()).getJSONObject("result");
			_place.setDetail(detail);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _place;
	}
	/*
	@Override
	protected ArrayList<Place> doInBackground(Void... arg0) {
		PlaceService service = new PlaceService("AIzaSyCpoGu09s2pQfc9RiaXyenVz08FpEJ_W3U");
		
		for(int i=0;i<_placesList.size();i++){
			
			Place place = _placesList.get(i);
			
			JSONObject detail;
			try {
				detail = service.findPlaceDetail(place.getReference()).getJSONObject("result");
				place.setDetail(detail);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return _placesList;
	}*/

}