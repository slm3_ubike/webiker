package edu.ntut.place;

import java.util.ArrayList;

import edu.ntut.ubike.R;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import edu.ntut.ubike.MainActivity;
import edu.ntut.ubike.Model;

public class GetPlaces extends AsyncTask<Void, Void, ArrayList<Place>> {
	private ProgressDialog _dialog;
	private String _places;
	private Model _model;
	private MainActivity _activity;
	private int _type;
	
	public GetPlaces(String places, Model model, MainActivity activity, int type) {
		_places = places;
		_model = model;
		_activity = activity;
		_type = type;
		_dialog = new ProgressDialog(activity);
	}

	@Override
	protected void onPostExecute(ArrayList<Place> result) {
		super.onPostExecute(result);
		for(int i=0;i<result.size();i++)
		{
			GetPlacesDetail getDetail = new GetPlacesDetail(result.get(i),_activity,_type);
			getDetail.execute();
		}
		_activity.EnableButton(_type);
		
		if (_dialog.isShowing())
            _dialog.dismiss();
	}

	@Override
	protected void onPreExecute() {
		this._dialog.setTitle("資料讀取中");
		this._dialog.setMessage("請稍候");
		this._dialog.setCanceledOnTouchOutside(false);
        this._dialog.show();
		super.onPreExecute();
	}

	@Override
	protected ArrayList<Place> doInBackground(Void... arg0) {	
		PlaceService service = new PlaceService(_activity.getString(R.string.BROWSER_KEY));
		LatLng userLocation = _model.getUserLocation();
		ArrayList<Place> findPlaces = service.findPlaces(userLocation.latitude,	userLocation.longitude, _places);
		return findPlaces;
	}

}