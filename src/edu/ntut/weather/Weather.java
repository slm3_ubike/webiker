package edu.ntut.weather;

import java.text.SimpleDateFormat;

public class Weather {

	String _locationName;
	String _weatherInfo;
	String _CI;
	int _maxTemperature;
	int _minTemperature;
	int _rainingRatio;
	SimpleDateFormat _startTime;
	SimpleDateFormat _endTime;

	public String getLocation() {
		return _locationName;
	}

	public void setLocation(String location) {
		this._locationName = location;
	}
	
	
	public String getWeatherInfo() {
		return _weatherInfo;
	}

	public void setWeatherInfo(String weatherInfo) {
		this._weatherInfo = weatherInfo;
	}
	
	public String getCI() {
		return _CI;
	}

	public void setCI(String CI) {
		this._CI = CI;
	}


	public int getMaxTemperature() {
		return _maxTemperature;
	}

	public void setMaxTemperature(int maxTemperature) {
		this._maxTemperature = maxTemperature;
	}

	public int getMinTemperature() {
		return _minTemperature;
	}

	public void setMinTemperature(int minTemperature) {
		this._minTemperature = minTemperature;
	}

	public int getRainingRatio() {
		return _rainingRatio;
	}

	public void setRainingRatio(int rainingRatio) {
		this._rainingRatio = rainingRatio;
	}
	
	
	public SimpleDateFormat getStartTime() {
		return _startTime;
	}

	public void setStartTime(SimpleDateFormat startTime) {
		this._startTime = startTime;
	}


	public SimpleDateFormat getEndTime() {
		return _endTime;
	}


	public void setEndTime(SimpleDateFormat endTime) {
		this._endTime = endTime;
	}

	
	
}
