package edu.ntut.weather;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.os.Debug;
import android.util.Log;
import android.util.Xml;


public class XmlParser {
	
	private static final String ns = null;
	   
    private ArrayList<Weather> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, "utf-8");
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }
    
    private ArrayList<Weather> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
    	ArrayList<Weather> entries = new ArrayList<Weather>();

    	
        //利用eventType來判斷目前分析到XML是哪一個部份
        int eventType = parser.getEventType();
        //XmlPullParser.END_DOCUMENT表示已經完成分析XML
        while(eventType != XmlPullParser.END_DOCUMENT)
        {
            //XmlPullParser.START_TAG表示目前分析到的是XML的Tag，如<title>
            if (eventType == XmlPullParser.START_TAG) {
              String name = parser.getName();
              if(name.equalsIgnoreCase("location"))
              {
                  readLocation(parser, entries);
              }
              //tv01.setText(tv01.getText() + ", " + name);
            }
            eventType = parser.next();
        }
    	
        return entries;
    }
    
    private void readLocation(XmlPullParser parser,ArrayList<Weather> list) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "location");
        String locationName = "";
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("name")) {
            	locationName = readName(parser);
            } else if (name.equals("weather-elements")) {
            	readWeatherElement(parser,list,locationName);
            }
        }
    }
    
    private void readWeatherElement(XmlPullParser parser,ArrayList<Weather> list,String location) throws XmlPullParserException, IOException 
    {
    	ArrayList<Weather> tempList = new ArrayList<Weather>();
    	for(int i = 0; i < 3; i++)
    	{
    		Weather weather = new Weather();
    		weather.setLocation(location);
    		tempList.add(weather);
    		list.add(weather);
    	}
        parser.require(XmlPullParser.START_TAG, ns, "weather-elements");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("Wx")) {
            	readWeatherInfo(parser,tempList);
            } else if (name.equals("MaxT")) {
            	readMaxTemperature(parser,tempList);
            } else if (name.equals("MinT")) {
            	readMinTemperature(parser,tempList);
            } else if (name.equals("CI")) {
            	readCI(parser,tempList);
            } else if (name.equals("PoP")) {
            	readRainingRatio(parser,tempList);
            }
        }
    }

    
    private void readWeatherInfo(XmlPullParser parser,ArrayList<Weather> list) throws IOException, XmlPullParserException 
    {
        parser.require(XmlPullParser.START_TAG, ns, "Wx");
        int index = 0;
        while(true)
        {
        	int type = parser.next();
        	if(type == XmlPullParser.END_TAG && parser.getName().equals("Wx"))
        	{
        		break;
        	}
        	
        	if(type == XmlPullParser.START_TAG && parser.getName().equals("text"))
        	{
                parser.require(XmlPullParser.START_TAG, ns, "text");
                list.get(index).setWeatherInfo(readText(parser));
                parser.require(XmlPullParser.END_TAG, ns, "text");
                index++;
        	}
        }
        
    }
    
    private void readCI(XmlPullParser parser,ArrayList<Weather> list) throws IOException, XmlPullParserException 
    {
        parser.require(XmlPullParser.START_TAG, ns, "CI");
        int index = 0;
        while(true)
        {
        	int type = parser.next();
        	if(type == XmlPullParser.END_TAG && parser.getName().equals("CI"))
        	{
        		break;
        	}
        	
        	if(type == XmlPullParser.START_TAG && parser.getName().equals("text"))
        	{
                parser.require(XmlPullParser.START_TAG, ns, "text");
                list.get(index).setCI(readText(parser));
                parser.require(XmlPullParser.END_TAG, ns, "text");
                index++;
        	}
        }
        
    }
    
    private void readMaxTemperature(XmlPullParser parser,ArrayList<Weather> list) throws IOException, XmlPullParserException 
    {
    	parser.require(XmlPullParser.START_TAG, ns, "MaxT");
        int index = 0;
        while(true)
        {
        	int type = parser.next();
        	if(type == XmlPullParser.END_TAG && parser.getName().equals("MaxT"))
        	{
        		break;
        	}
        	
        	if(type == XmlPullParser.START_TAG && parser.getName().equals("value"))
        	{
                parser.require(XmlPullParser.START_TAG, ns, "value");
                list.get(index).setMaxTemperature(Integer.parseInt(readText(parser)));
                parser.require(XmlPullParser.END_TAG, ns, "value");
                index++;
        	}
        }
        
    }
    

    private void readMinTemperature(XmlPullParser parser,ArrayList<Weather> list) throws IOException, XmlPullParserException 
    {
    	parser.require(XmlPullParser.START_TAG, ns, "MinT");
        int index = 0;
        while(true)
        {
        	int type = parser.next();
        	if(type == XmlPullParser.END_TAG && parser.getName().equals("MinT"))
        	{
        		break;
        	}
        	
        	if(type == XmlPullParser.START_TAG && parser.getName().equals("value"))
        	{
                parser.require(XmlPullParser.START_TAG, ns, "value");
                list.get(index).setMinTemperature(Integer.parseInt(readText(parser)));
                parser.require(XmlPullParser.END_TAG, ns, "value");
                index++;
        	}
        }
        
    }

    private void readRainingRatio(XmlPullParser parser,ArrayList<Weather> list) throws IOException, XmlPullParserException 
    {
    	parser.require(XmlPullParser.START_TAG, ns, "PoP");
        int index = 0;
        while(true)
        {
        	int type = parser.next();
        	if(type == XmlPullParser.END_TAG && parser.getName().equals("PoP"))
        	{
        		break;
        	}
        	
        	if(type == XmlPullParser.START_TAG && parser.getName().equals("value"))
        	{
                parser.require(XmlPullParser.START_TAG, ns, "value");
                list.get(index).setRainingRatio(Integer.parseInt(readText(parser)));
                parser.require(XmlPullParser.END_TAG, ns, "value");
                index++;
        	}
        }
        
    }
    
    private String readName(XmlPullParser parser) throws IOException, XmlPullParserException {
        String name = "";
        
        parser.require(XmlPullParser.START_TAG, ns, "name");
        name = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "name");
        
        return name;
    }
    
    // For the tags title and summary, extracts their text values.
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
    
	
	public List<Weather> getWeatherInformations() {
		URL url;
		InputStream is = null;
		List<Weather> weather = null;
		
		try {
			
			url = new URL("http://opendata.cwb.gov.tw/opendata/MFC/F-C0032-001.xml");
			is = url.openStream();  
			try {
				weather = parse(is);
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return weather;
	}
	
	
}
