package edu.ntut.JsonHelper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class JsonHelper {
	static List<Station> stations = new ArrayList<Station>();
	
	public JsonHelper(){
		stations.clear();
	}
	public List<Station> getUbikeDataWithJsonFormat() {
		URL url;
		InputStream is = null;
		
		
		try {
			
			url = new URL("http://opendata.dot.taipei.gov.tw/opendata/gwjs_cityhall.json");
			is = url.openStream();  
			String jsonStr = getStringFromInputStream(is);
			JsonParser parser = new JsonParser();
            JsonObject jsonObject = (JsonObject)parser.parse(jsonStr); 
            
            //System.out.println(jsonObject);
            JsonArray array = jsonObject.getAsJsonArray("retVal"); 
            
            for(int i = 0 ; i < array.size() ; i++){
            	JsonObject element = array.get(i).getAsJsonObject();
            	Station station = new Station();
            	station.setItemId(element.get("iid").getAsString());
            	station.setVersion(element.get("sv").getAsString());
            	station.setStartDay(element.get("sd").getAsString());
            	station.setVersionType(element.get("vtyp").getAsString());
            	station.setNo(element.get("sno").getAsString());
            	station.setNameChinese(element.get("sna").getAsString());
            	station.setIp(element.get("sip").getAsString());
            	station.setTotalBike(element.get("tot").getAsString());
            	station.setCurrentBike(element.get("sbi").getAsString());
            	station.setAreaChinese(element.get("sarea").getAsString());
            	station.setModifiedDay(element.get("mday").getAsString());
            	station.setLatitude(element.get("lat").getAsString());
            	station.setLongitude(element.get("lng").getAsString());
            	station.setAddressChinese(element.get("ar").getAsString());
            	station.setAreaEnglish(element.get("sareaen").getAsString());
            	station.setNameEnglish(element.get("snaen").getAsString());
            	station.setAddressEnglish(element.get("aren").getAsString());
            	station.setBemp(element.get("bemp").getAsString());
            	stations.add(station);
            }	
               
            //printData();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return stations;
	}
	
	private static void printData(){
		for(int i = 0 ; i < stations.size() ; i++){
			System.out.println("ItemId:" + stations.get(i).getItemId());
			System.out.println("Version:" + stations.get(i).getVersion());
			System.out.println("Start Day:" + stations.get(i).getStartDay());
			System.out.println("Version Type:" + stations.get(i).getVersionType());
			System.out.println("No:" + stations.get(i).getNo());
			System.out.println("Chinese Name:" + stations.get(i).getNameChinese());
			System.out.println("IP:" + stations.get(i).getIp());
			System.out.println("Total Bikes:" + stations.get(i).getTotalBike());
			System.out.println("Current Bikes:" + stations.get(i).getCurrentBike());
			System.out.println("Chinese Area:" + stations.get(i).getAreaChinese());
			System.out.println("Modified Day:" + stations.get(i).getModifiedDay());
			System.out.println("Latitude:" + stations.get(i).getLatitude());
			System.out.println("Longitude:" + stations.get(i).getLongitude());
			System.out.println("Chinese Address:" + stations.get(i).getAddressChinese());
			System.out.println("English Area:" + stations.get(i).getAreaEnglish());
			System.out.println("English Name:" + stations.get(i).getNameEnglish());
			System.out.println("English Address:" + stations.get(i).getAddressEnglish());
			System.out.println("Bemp:" + stations.get(i).getBemp());
			System.out.println("-----------------------------------------------------------------");
		}
	}
	
	// convert InputStream to String
	private static String getStringFromInputStream(InputStream is) {
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}
	
}
