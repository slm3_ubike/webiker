package edu.ntut.JsonHelper;

import com.google.android.gms.maps.model.LatLng;

import edu.ntut.place.Place;

public class Station {
	String itemId;
	String version;  //場站版本
	String startDay;  //啟用時間
	String versionType;  //版本別
	String no;  //場站代號
	String nameChinese;  //場站名稱
	String ip;  //場站 IP
	String totalBike;  //場站的總停車格
	String currentBike;  //場站的目前車輛數
	String areaChinese;  //場站區域
	String modifiedDay; //資料更新時間
	String longitude; //經度
	String latitude; //緯度
	String addressChinese;  //地址
	String areaEnglish;  //場站區域英文名稱
	String nameEnglish;  //場站英文名稱
	String addressEnglish;  //英文地址
	String bemp; //空位數量
	LatLng latLng;
	Place place;
	
    public Station(){
		place = new Place();
	}
    
    public LatLng getLatLng()
    {
    	if(latLng == null)
    	{
        	double lat = Double.parseDouble(latitude);
        	double lng = Double.parseDouble(longitude);
        	latLng = new LatLng(lat,lng);
    	}
    	
    	return latLng;
    }
	
	public String getItemId() {
		return itemId;
	}


	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public String getStartDay() {
		return startDay;
	}


	public void setStartDay(String startDay) {
		this.startDay = startDay;
	}


	public String getVersionType() {
		return versionType;
	}


	public void setVersionType(String versionType) {
		this.versionType = versionType;
	}


	public String getNo() {
		return no;
	}


	public void setNo(String no) {
		this.no = no;
	}


	public String getNameChinese() {
		return nameChinese;
	}


	public void setNameChinese(String nameChinese) {
		this.nameChinese = nameChinese;
		place.setName(nameChinese);
	}


	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}


	public String getTotalBike() {
		return totalBike;
	}


	public void setTotalBike(String totalBike) {
		this.totalBike = totalBike;
	}


	public String getCurrentBike() {
		return currentBike;
	}


	public void setCurrentBike(String currentBike) {
		this.currentBike = currentBike;
	}


	public String getAreaChinese() {
		return areaChinese;
	}


	public void setAreaChinese(String areaChinese) {
		this.areaChinese = areaChinese;
	}


	public String getModifiedDay() {
		return modifiedDay;
	}


	public void setModifiedDay(String modifiedDay) {
		this.modifiedDay = modifiedDay;
	}


	public String getLongitude() {
		return longitude;
	}


	public void setLongitude(String longitude) {
		this.longitude = longitude;
		place.setLatitude(Double.parseDouble(longitude));
	}


	public String getLatitude() {
		return latitude;
	}


	public void setLatitude(String latitude) {
		this.latitude = latitude;
		place.setLatitude(Double.parseDouble(latitude));
	}


	public String getAddressChinese() {
		return addressChinese;
	}


	public void setAddressChinese(String addressChinese) {
		this.addressChinese = addressChinese;
	}


	public String getAreaEnglish() {
		return areaEnglish;
	}


	public void setAreaEnglish(String areaEnglish) {
		this.areaEnglish = areaEnglish;
	}


	public String getNameEnglish() {
		return nameEnglish;
	}


	public void setNameEnglish(String nameEnglish) {
		this.nameEnglish = nameEnglish;
	}


	public String getAddressEnglish() {
		return addressEnglish;
	}


	public void setAddressEnglish(String addressEnglish) {
		this.addressEnglish = addressEnglish;
	}

	public void setBemp(String bemp){
		this.bemp = bemp;
	}
	
	public String getBemp(){
		return bemp;
	}
	
	public Place getPlace(){
		return place;
	}
	
}
