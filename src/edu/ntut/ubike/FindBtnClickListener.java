package edu.ntut.ubike;

import edu.ntut.JsonHelper.Station;

import java.util.ArrayList;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.navdrawer.SimpleSideDrawer;

import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.SearchView.OnCloseListener;
import android.widget.TextView;

public class FindBtnClickListener implements Button.OnClickListener{
	private Model _model;
	private MainActivity _activity;
	private SearchView _search;
	private ListView _ubikeList;
	private SimpleSideDrawer sideMenu_UBikeStations;
	
	
	
	
	public FindBtnClickListener(Model model, MainActivity activity){
		_model = model;
		_activity = activity;
		
		 //initialize simple side drawer
        sideMenu_UBikeStations = new SimpleSideDrawer(activity);
        sideMenu_UBikeStations.setRightBehindContentView(R.layout.side_menu);
        _ubikeList = (ListView) sideMenu_UBikeStations.findViewById(R.id.ubike_list_view);
	}
	
	@Override
	public void onClick(View v) {
		initUbikeListInfo();
		if(_search == null) {
			_search = (SearchView) sideMenu_UBikeStations.findViewById(R.id.input_search);
		}
		
		_search.setOnCloseListener(new OnCloseListener() {
	        @Override
	        public boolean onClose() {
	            return false;
	        }
	    });
		
		_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean onQueryTextChange(String newText) {
				if (TextUtils.isEmpty(newText)) {  
					_ubikeList.clearTextFilter();  
		        } else {    
		        	_ubikeList.setFilterText(newText.toString());  
		        }  
		        return false;
			}
		});
		sideMenu_UBikeStations.toggleRightDrawer();
	}
	
	private void initUbikeListInfo(){
		ArrayList<String> listOfUbike = new ArrayList<String>();
		ArrayAdapter<String> aadapter;
		
		for(Station station : _model.getUbikeStations()){
			listOfUbike.add(station.getNameChinese());
		}
		
		aadapter = new ArrayAdapter<String>(_activity, 
		android.R.layout.simple_expandable_list_item_1,listOfUbike);
		
		_ubikeList.setAdapter(aadapter);
		_ubikeList.setTextFilterEnabled(true);		
		
		_ubikeList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view,
					int position, long arg) {
				// TODO Auto-generated method stub
				ListView listView = (ListView) adapter;
				String str = listView.getItemAtPosition(position).toString();
				
				for(PlaceMarker place : _activity.getPlaceMarkerList()){
		    		if(place.getTitle().equals(str)){
		    			_activity._map.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getMarker().getPosition(), 16));
		    			place.getMarker().showInfoWindow();
		    			sideMenu_UBikeStations.toggleLeftDrawer();
		    		}
		    	}
			}
		});	
	}
	

}
