package edu.ntut.ubike;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.view.View;

import com.google.android.gms.maps.model.Marker;

public class PlaceDetailClickListener extends OnInfoWindowElemTouchListener{
	private MainActivity _activity;
	private ArrayList<String> photoReferenceList = new ArrayList<String>();
	public PlaceDetailClickListener(View view, MainActivity activity) {
		super(view);
		_activity = activity;
	}

	@Override
    protected void onClickConfirmed(View v, Marker marker) {
		for(PlaceMarker placeMarker: _activity.getPlaceMarkerList()){
			if(placeMarker.getMarker().equals(marker)){
				Intent intent = new Intent();
				intent.setClass(_activity.getApplicationContext(), PlaceDetailActivity.class);
				try {
					if(placeMarker.getPlace().getDetail() != null){
						intent.putExtra("name", (String)placeMarker.getPlace().getDetail().get("name"));
						intent.putExtra("address", (String)placeMarker.getPlace().getDetail().get("formatted_address"));
						intent.putExtra("phone", (String)placeMarker.getPlace().getDetail().get("formatted_phone_number"));
						//intent.putExtra("photoReference", ((JSONObject)(placeMarker.getPlace().getPhotos().get(0))).getString("photo_reference"));
						if(placeMarker.getPlace().getPhotos() != null){
							for(int i=0; i<placeMarker.getPlace().getPhotos().length(); i++){
								photoReferenceList.add(((JSONObject)(placeMarker.getPlace().getPhotos().get(i))).getString("photo_reference"));
							}
						}
						intent.putStringArrayListExtra("photoReferenceList", photoReferenceList);
						_activity.startActivity(intent);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
