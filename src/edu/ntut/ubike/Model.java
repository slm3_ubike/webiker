package edu.ntut.ubike;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.StrictMode;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import edu.ntut.JsonHelper.JsonHelper;
import edu.ntut.JsonHelper.Station;
import edu.ntut.weather.Weather;
import edu.ntut.weather.XmlParser;

public class Model {
	private Context _context;
	List<Station> _stations;
	List<Weather> _weathers;
	List<PlaceMarker> _markList = new ArrayList<PlaceMarker>();
	LatLng latlng;
	private boolean _isClick = false;
	
	public Model(Context context){
		_context = context;
	}

	public LatLng getUserLocation() {
		Criteria criteria = new Criteria();
		LocationManager locationManager = (LocationManager) _context.getSystemService(Context.LOCATION_SERVICE);
		if(locationManager.getBestProvider(criteria, true) != null){
			Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true));
			if(location != null){
		    	double lat = location.getLatitude();
		    	double lng = location.getLongitude();
		    	return new LatLng(lat, lng);
			}
			else{
				//Toast.makeText(_context, "無法取得目前位置, 請確認定位服務是否開啟", Toast.LENGTH_SHORT).show();
	    		return new LatLng(25.04744, 121.516098);
			}
    	}
    	else{
    		//Toast.makeText(_context, "無法取得目前位置, 請確認定位服務是否開啟", Toast.LENGTH_SHORT).show();
    		return new LatLng(25.04744, 121.516098);
    	}
	}



	public List<Weather> getWeatherInformation() {
		if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
		XmlParser xmlParser = new XmlParser();
		_weathers = xmlParser.getWeatherInformations();
    	return _weathers;
	}
	
	public List<Station> getUbikeStations() {
		if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
		JsonHelper jsonHelper = new JsonHelper();
    	_stations = jsonHelper.getUbikeDataWithJsonFormat();
    	return _stations;
	}
	
	public Station getNearestStation(LatLng myLatLng) {
		double nearestDiatance = 360.0*360.0;
		Station nearestStation = null;
		
		for(Station station : getUbikeStations()){
			double ubikeLat = Double.parseDouble(station.getLatitude());
			double ubikeLng = Double.parseDouble(station.getLongitude());
			double distance;
			distance = Math.sqrt(Math.pow(ubikeLat-myLatLng.latitude,2) + 
								 Math.pow(ubikeLng-myLatLng.longitude,2));
			System.out.print(distance);
			if(distance < nearestDiatance){
				nearestDiatance = distance;
				nearestStation = station;
			}
		}
		return nearestStation;
	}
	
	/**
	 * 當點擊marker一剎那，抓取該marker的座標，並設定給latlng，以便提供街景
	 * @param placeMarker
	 */
	public void setLatLngForStreetView(PlaceMarker placeMarker) {
		for(int i = 0; i < _markList.size(); i++) {
			if(placeMarker.equals(_markList.get(i))) {	
				latlng = _markList.get(i).getMarker().getPosition();
				break;
			}
		}	
	}
	
	/**
	 * 得到目前所有的marker
	 * @param placeMarker
	 */
	public void getAllMarker(PlaceMarker placeMarker) {
		_markList.add(placeMarker);
	}

	
	/**
	 * 保留剩下ubike marker的List
	 * @param placeMarker
	 */
	public void storeUbikeMarker(ArrayList<PlaceMarker> placeMarker) {
		_markList = placeMarker;
	}
	
	public boolean getVisibility(){
		return _isClick;
	}
	
	public void setVisibility(boolean flag){
		this._isClick = flag;
	}
	
	
	
}
