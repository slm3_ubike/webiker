package edu.ntut.ubike;

import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.Marker;

public class InfoWindowClickListener implements OnInfoWindowClickListener{
	
	private MainActivity _activity;
	
	public InfoWindowClickListener(MainActivity activity) {
		this._activity = activity;	
	}
	
	
	/**
	 * 點擊Infowindows則跳過去街景畫面
	 */
	@Override
	public void onInfoWindowClick(Marker marker) {
		_activity.streetView();
	}
}
