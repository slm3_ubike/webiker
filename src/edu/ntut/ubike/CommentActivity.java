package edu.ntut.ubike;

import org.json.JSONException;
import org.json.JSONObject;

import edu.ntut.place.PlaceService;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RatingBar;
import android.widget.TextView;

public class CommentActivity extends Activity{
	 private String reference;
	 private String url;
	 private int starNumber = 24;
	 private String mobilePage = "https://plus.google.com/app/basic/";
	 private String endUrl = "&source=apppromo";
	 
	 protected void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
	     setContentView(R.layout.comment_layout);
	     
	     Intent intent = getIntent(); 
	     reference = intent.getStringExtra("Reference");
	     PlaceService services = new PlaceService(getResources().getString(R.string.BROWSER_KEY));
	     JSONObject object = services.findPlaceDetail(reference);
	     
	     try {
	    	 
			url = object.getJSONObject("result").getString("url");
			Log.i("Url:", url);
			String tmp ="";
			tmp = url.substring(starNumber, url.length());
			url = mobilePage + tmp + endUrl;
		
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	     
	     WebView myBrowser=(WebView)findViewById(R.id.webView1);       
	     WebSettings websettings = myBrowser.getSettings();  
	     websettings.setUseWideViewPort(true);
	     websettings.setLoadWithOverviewMode(true);
	     websettings.setSupportZoom(true);  
	     websettings.setBuiltInZoomControls(true);       
	     myBrowser.loadUrl(url);    
	 }
}
