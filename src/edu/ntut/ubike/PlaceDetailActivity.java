package edu.ntut.ubike;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class PlaceDetailActivity extends Activity{
	private TextView _name;
	private TextView _address;
	private TextView _phone;
	private ImageView _image;
	private ArrayList<String> _photoReference;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_detail);
        _name = (TextView) this.findViewById(R.id.name);
        _address = (TextView) this.findViewById(R.id.address);
        _phone = (TextView) this.findViewById(R.id.phone);
        _image = (ImageView) this.findViewById(R.id.imageView1);
        showInfo();
	}
	
	public void showInfo(){
		_name.setText(this.getIntent().getStringExtra("name"));
		_address.setText(this.getIntent().getStringExtra("address"));
		_phone.setText(this.getIntent().getStringExtra("phone"));
		_photoReference = this.getIntent().getStringArrayListExtra("photoReferenceList");
		if(_photoReference != null){
			for(int i=0; i<_photoReference.size(); i++){
				_image.setImageDrawable(loadImageFromURL("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + _photoReference.get(i)+ "&sensor=true&key=" + getString(R.string.BROWSER_KEY)));
			}
		}
        
	}
	
	public Drawable loadImageFromURL(String url){
        try{
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable draw = Drawable.createFromStream(is, "src");
            return draw;
        }catch (Exception e) {
            return null;
        }
    }
}
