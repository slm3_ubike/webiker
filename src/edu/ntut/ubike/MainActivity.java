package edu.ntut.ubike;

import java.util.ArrayList;

import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusOneButton;

import edu.ntut.JsonHelper.Station;
import edu.ntut.googleDirection.DirectionService;
import edu.ntut.googleDirection.GetDirectionJson;
import edu.ntut.place.Place;
import edu.ntut.weather.Weather;
import edu.ntut.weather.XmlParser;

public class MainActivity extends Activity implements OnMapLongClickListener{//, ConnectionCallbacks, OnConnectionFailedListener{
	GoogleMap _map;
	private Model _model;
	private Context _context;
	private TextView _weatherText;
	private ImageButton _eatBtn;
	private ImageButton _artBtn;
	private ImageButton _busBtn;
	private ImageButton _sevenBtn;
	private ImageButton _mrtBtn;
	private ImageButton _findBtn;
	private TextView _direcTV;
	
	private static final String URL = "https://developers.google.com/+";
	private ProgressDialog mConnectionProgressDialog;
	private static final int REQUEST_CODE_RESOLVE_ERR = 9000;	
	private ConnectionResult mConnectionResult;
	private static final int PLUS_ONE_REQUEST_CODE = 0;
	private PlusClient mPlusClient;
	private PlusOneButton mPlusOneButton;
	
	private ArrayList<PlaceMarker> _placeMarkerList;
	private ArrayList<Polyline> _routePolyLine;
	
	public GoogleMap getMap(){
		return _map;
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //View Object
        
        _map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        _map.setMyLocationEnabled(true);
		_map.setOnMapLongClickListener(this);
		_weatherText = (TextView) this.findViewById(R.id.weatherTextView);
		_eatBtn = (ImageButton) this.findViewById(R.id.eatButton);
        _artBtn = (ImageButton) this.findViewById(R.id.artButton);
        _busBtn = (ImageButton) this.findViewById(R.id.busButton);
        _sevenBtn = (ImageButton) this.findViewById(R.id.sevenButton);
        _mrtBtn = (ImageButton) this.findViewById(R.id.mrtButton);
        _findBtn = (ImageButton) this.findViewById(R.id.findButton);
        _direcTV = (TextView) this.findViewById(R.id.dirtview);
        //Model
        _context = this.getApplicationContext();
        _model = new Model(_context);
        _eatBtn.setOnClickListener(new PlaceBtnClickListener(_model, this, R.string.EAT));
        _artBtn.setOnClickListener(new PlaceBtnClickListener(_model, this,R.string.ART));
        _busBtn.setOnClickListener(new PlaceBtnClickListener(_model, this,R.string.BUS));        
        _sevenBtn.setOnClickListener(new PlaceBtnClickListener(_model, this,R.string.SEVEN));
        _mrtBtn.setOnClickListener(new PlaceBtnClickListener(_model, this,R.string.MRT));
        _findBtn.setOnClickListener(new FindBtnClickListener(_model, this));
        _map.setOnMarkerClickListener(new MarkerClickListener(this, _model));
        //_map.setOnInfoWindowClickListener(new InfoWindowClickListener(this));
        _placeMarkerList = new ArrayList<PlaceMarker>();
        _routePolyLine = new ArrayList<Polyline>();

    	_map.setInfoWindowAdapter(new MarkerInfoAdapter(this,_model));
      
       //mPlusClient = new PlusClient.Builder(this, this, this).build();
       // mPlusOneButton = (PlusOneButton) findViewById(R.id.plus_one_button);
    	
    	showWeatherInfo();
        showUserLocation();
        showUbikeStations();
       //showNearStation();
    }
    
    public void showWeatherInfo()
    {
    	Weather currentWeather = _model.getWeatherInformation().get(0);
    	String location = currentWeather.getLocation() + " 天氣狀況\n";
    	String temperature = "氣溫：" + currentWeather.getMinTemperature() + "°C ~ " + currentWeather.getMaxTemperature() + "°C \n";
    	String rainingRatio = "降雨機率；" + currentWeather.getRainingRatio() + "%\n";
    	String weatherInfo = currentWeather.getWeatherInfo();
    	
    	_weatherText.setText(location + temperature + rainingRatio + weatherInfo);
    	
    }
    
    
    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dp * scale + 0.5f);
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	showUserLocation();
    	//mPlusOneButton.initialize(URL, PLUS_ONE_REQUEST_CODE);
    }
    
    public void showUserLocation() {
    	_map.moveCamera(CameraUpdateFactory.newLatLngZoom(_model.getUserLocation(), 16));
    }
    
    
    public void showUbikeStations() {
    	for(Station station : _model.getUbikeStations()){
    		if(station.getLatitude() != "" && station.getLongitude() != ""){

	    		double lat = Double.parseDouble(station.getLatitude());
		    	double lng = Double.parseDouble(station.getLongitude());
	
		    	LatLng location = new LatLng(lat, lng);
		    	String title = station.getNameChinese();
		    	String snippet = " 目前車輛數: " + station.getCurrentBike() + "    空位數量: " + station.getBemp();
		    	MarkerOptions markerOpt = new MarkerOptions().position(location).title(title).snippet(snippet);
		    	markerOpt.icon(BitmapDescriptorFactory.fromResource(R.drawable.bike));
		    	Marker marker = _map.addMarker(markerOpt);
    			PlaceMarker placeMarker = new PlaceMarker(R.string.UBIKE, title, snippet, marker);
    			placeMarker.setPlace(station.getPlace());
    			_placeMarkerList.add(placeMarker);
    			_model.getAllMarker(placeMarker);
    		}
    	}
    }
    
    public void addEatLocation(Place place) {
    	double lat = place.getLatitude();
    	double lng = place.getLongitude();
    	LatLng location = new LatLng(lat, lng);
    	String staName = place.getName();
    	MarkerOptions markerOpt = new MarkerOptions().position(location).title(staName);
    	markerOpt.icon(BitmapDescriptorFactory.fromResource(R.drawable.eat));
    	Marker marker = _map.addMarker(markerOpt);
    	PlaceMarker placeMarker = new PlaceMarker(R.string.EAT, staName, "", marker);
    	placeMarker.setPlace(place);
		_placeMarkerList.add(placeMarker);
		_model.getAllMarker(placeMarker);
    }
    
    public void showEatLocation(ArrayList<Place> locations) {
    	for(Place place : locations){
    		addEatLocation(place);
    	}
    }
    
    public void addArtLocation(Place place) {
    	double lat = place.getLatitude();
    	double lng = place.getLongitude();
    	LatLng location = new LatLng(lat, lng);
    	String staName = place.getName();
    	MarkerOptions markerOpt = new MarkerOptions().position(location).title(staName);
    	markerOpt.icon(BitmapDescriptorFactory.fromResource(R.drawable.art));
    	Marker marker = _map.addMarker(markerOpt);
    	
    	PlaceMarker placeMarker = new PlaceMarker(R.string.ART, staName, "", marker);
    	placeMarker.setPlace(place);
		_placeMarkerList.add(placeMarker);
		_model.getAllMarker(placeMarker);
    }
    
    public void showArtLocation(ArrayList<Place> locations) {
    	for(Place place : locations){
    		addArtLocation(place);
    	}
    }
    
    public void addBusLocation(Place place) {
    	double lat = place.getLatitude();
    	double lng = place.getLongitude();
    	LatLng location = new LatLng(lat, lng);
    	String staName = place.getName();
    	MarkerOptions markerOpt = new MarkerOptions().position(location).title("公車站牌名稱：" + staName);
    	markerOpt.icon(BitmapDescriptorFactory.fromResource(R.drawable.bus));
    	Marker marker = _map.addMarker(markerOpt);
    	
    	PlaceMarker placeMarker = new PlaceMarker(R.string.BUS, staName, "", marker);
    	placeMarker.setPlace(place);
		_placeMarkerList.add(placeMarker);
		_model.getAllMarker(placeMarker);
    }
    
    public void showBusLocation(ArrayList<Place> locations){
    	for(Place place : locations){
    		addBusLocation(place);
    	}
    }

    
    public void addSevenLocation(Place place) {
    	double lat = place.getLatitude();
    	double lng = place.getLongitude();
    	LatLng location = new LatLng(lat, lng);
    	String staName = place.getName();
    	MarkerOptions markerOpt = new MarkerOptions().position(location).title(staName);
    	markerOpt.icon(BitmapDescriptorFactory.fromResource(R.drawable.store));
    	Marker marker = _map.addMarker(markerOpt);
    	PlaceMarker placeMarker = new PlaceMarker(R.string.SEVEN, staName, "", marker);
    	placeMarker.setPlace(place);
		_placeMarkerList.add(placeMarker);
		_model.getAllMarker(placeMarker);
    }
    
    public void showSevenLocation(ArrayList<Place> locations) {
    	for(Place place : locations){
    		addSevenLocation(place);
    	}
    }
    
    public void addMRTLocation(Place place) {
    	double lat = place.getLatitude();
    	double lng = place.getLongitude();
    	LatLng location = new LatLng(lat, lng);
    	String staName = place.getName();
    	MarkerOptions markerOpt = new MarkerOptions().position(location).title(staName);
    	markerOpt.icon(BitmapDescriptorFactory.fromResource(R.drawable.train));
    	Marker marker = _map.addMarker(markerOpt);
    	PlaceMarker placeMarker = new PlaceMarker(R.string.MRT, staName, "", marker);
    	placeMarker.setPlace(place);
		_placeMarkerList.add(placeMarker);
		_model.getAllMarker(placeMarker);
    }
    
    public void showMRTLocation(ArrayList<Place> locations) {
    	for(Place place : locations){
    		addMRTLocation(place);
    	}
    	
    }
    
    public void dispearMapMarker(int type) {
    	for(PlaceMarker marker: _placeMarkerList){
        	if(marker.getType() == type){
        		marker.getMarker().remove();
        		marker.remove(); 
        	}
        }
    	_model.storeUbikeMarker(_placeMarkerList);
    }
    
    public void showNearStation() {
    	DirectionService directionService = new DirectionService();
    	new GetDirectionJson(this, Color.argb(120, 0, 0, 255), 8).execute(directionService.getDirectionsUrl(_model.getUserLocation(), _model.getNearestStation(_model.getUserLocation()).getLatLng()));
    }
    
    public void addRoute(PolylineOptions route) {
    	_routePolyLine.add(_map.addPolyline(route));
    }
    
    public void addText(ArrayList<String> directString) {
    	_direcTV.setTextSize(16);
    	for(int i = 0; i<directString.size(); i++){		
    		_direcTV.append((i+1) +". " + directString.get(i) + "\n");
    	}
    	_direcTV.append("------------------\n");
    }
    
    public void clearRoute() {
    	for(Polyline polyline : _routePolyLine) {
    		polyline.remove();
    	}
    }

    public ArrayList<PlaceMarker> getPlaceMarkerList() {
    	return _placeMarkerList;
    }
    
	@Override
	public void onMapLongClick(LatLng point) {

		View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.longclick_layout, null);
		_map.addMarker(new MarkerOptions()
		.position(point)
		.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, marker))));	
	}
	
	
	// Convert a view to bitmap
	public static Bitmap createDrawableFromView(Context context, View view) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.buildDrawingCache();
		Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
 
		Canvas canvas = new Canvas(bitmap);
		view.draw(canvas);
 
		return bitmap;
	}
	
	/**
	 * Stree View
	 */
	public void streetView(){
    	double lat = _model.latlng.latitude;
		double lng = _model.latlng.longitude;
		String latString = String.valueOf(lat);
		String lognString = String.valueOf(lng);
		String strUri = "google.streetview:cbll=" + latString + ", " + lognString;
    	Toast.makeText(MainActivity.this, strUri, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));
        startActivity(intent);
    }
	
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);
	    return true;
	} 
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		String url = "http://taipei.youbike.com.tw/cht/f21.html?classid=";
		switch (item.getItemId()) {
			case R.id.action_fixbike:
				try{
					intent = new Intent("android.intent.action.DIAL");
					intent.setData(Uri.parse("tel:1999"));
					startActivity(intent);
				}
				catch(ActivityNotFoundException e){
					Toast.makeText(_context, "平板不支援撥打電話功能", Toast.LENGTH_LONG).show();
				}
				break;
			case R.id.action_operating:
				url += "25cf21dcb0a64d5fc23391fb5c0e2701";
				intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(url));
				startActivity(intent);
				break;
			case R.id.action_urgent:
				url += "2e655556ffd214c0b9c258d2f46ba706";
				intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(url));
				startActivity(intent);
				break;
			case R.id.action_aboutteam:
				intent = new Intent(MainActivity.this, AboutActivity.class);
				startActivity(intent);
				break;
			default:
				break;
		}
		return true;
	}

	
	/**
	 * PlusClient initial
	 */
//	@Override
//	public void onConnectionFailed(ConnectionResult result) {
//		if (mConnectionProgressDialog.isShowing()) {
//	        // The user clicked the sign-in button already. Start to resolve
//	        // connection errors. Wait until onConnected() to dismiss the
//	        // connection dialog.
//	        if (result.hasResolution()) {
//	          try {
//	                   result.startResolutionForResult(this, REQUEST_CODE_RESOLVE_ERR);
//	           } catch (SendIntentException e) {
//	                   mPlusClient.connect();
//	           }
//	        }
//	      }
//	      // Save the result and resolve the connection failure upon a user click.
//	      mConnectionResult = result;
//	}
//
//	@Override
//	public void onConnected(Bundle connectionHint) {
//		String accountName = mPlusClient.getAccountName();
//        Toast.makeText(this, accountName + " is connected.", Toast.LENGTH_LONG).show();
//	}
//
//	@Override
//	public void onDisconnected() {
//		Log.d("mainActivity", "disconnected");	
//	}
	
	public void EnableButton(int id)
	{
		switch(id)
		{
			case R.string.EAT:
				_eatBtn.setEnabled(true);
				break;
			
			case R.string.BUS:
				_busBtn.setEnabled(true);
				break;
			
			case R.string.SEVEN:
				_sevenBtn.setEnabled(true);
				break;
				
			case R.string.ART:
				_artBtn.setEnabled(true);
				break;
				
			case R.string.MRT:
				_mrtBtn.setEnabled(true);
				break;
		}
	}
	
	public TextView getTextView(){
		return this._direcTV;
	}
}
