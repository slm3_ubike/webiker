package edu.ntut.ubike;

import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import edu.ntut.JsonHelper.Station;
import edu.ntut.googleDirection.DirectionService;
import edu.ntut.googleDirection.GetDirectionJson;
import edu.ntut.googleDirection.GetTimeAndDistanceJson;
import edu.ntut.place.Place;

public class MarkerInfoAdapter implements InfoWindowAdapter {
	private View _myMarkerView;
	private MainActivity _activity;
	private Model _model;

	private ViewGroup infoWindow;
    private TextView nameText;
    private TextView phoneText;
    private TextView addressText;
    private TextView distanceText;
    private TextView otherText;
    private Button moreDetailBtn;
    private TextView directionText;
    private Button commentBtn;
    private Button destinationBtn;
    private SlidingDrawer nevigationDrawer;
    private OnInfoWindowElemTouchListener moreDetailButtonListener;
    private OnInfoWindowElemTouchListener destinationButtonListener;
    private OnInfoWindowElemTouchListener commentButtonListener;
    private final MapWrapperLayout mapWrapperLayout;
    private String reference;
	
	MarkerInfoAdapter(MainActivity activity,Model model) {
		_activity = activity;
		_model = model;
		_myMarkerView = activity.getLayoutInflater().inflate(R.layout.marker_infowindow, null); 
				
		mapWrapperLayout = (MapWrapperLayout) _activity.findViewById(R.id.map_relative_layout);
		
		// MapWrapperLayout initialization
		// 39 - default marker height
		// 20 - offset between the default InfoWindow bottom edge and it's content bottom edge 
		mapWrapperLayout.init(_activity.getMap(), getPixelsFromDp(_activity, 39 + 20));
		
		this.infoWindow = (ViewGroup)_activity.getLayoutInflater().inflate(R.layout.marker_infowindow, null);
		 
		this.addressText = (TextView)infoWindow.findViewById(R.id.addressText);
		this.phoneText = (TextView)infoWindow.findViewById(R.id.phoneText);
		this.nameText = (TextView)infoWindow.findViewById(R.id.nameText);
		this.otherText = (TextView)infoWindow.findViewById(R.id.othersText);
		this.distanceText = (TextView)infoWindow.findViewById(R.id.distanceText);
		this.destinationBtn = (Button)infoWindow.findViewById(R.id.destinationButton);
		this.moreDetailBtn = (Button)infoWindow.findViewById(R.id.moreDetailButton);
		this.commentBtn = (Button)infoWindow.findViewById(R.id.commentButton);
		this.nevigationDrawer = (SlidingDrawer)activity.findViewById(R.id.bottom);
		
		this.destinationButtonListener = new OnInfoWindowElemTouchListener(destinationBtn) 
		{
		    @Override
		    protected void onClickConfirmed(View v, Marker marker) {
		        // Here we can perform some action triggered after clicking the button_activity.clearRoute();
		    	directionText = _activity.getTextView();
		    	directionText.setText("");
		    	_activity.clearRoute();
				marker.hideInfoWindow();
				Station nearUserStation = _model.getNearestStation(_model.getUserLocation());
				Station nearTargetLocationStation = _model.getNearestStation(marker.getPosition());
				DirectionService directionService = new DirectionService();
				new GetDirectionJson(_activity, Color.argb(120, 0, 0, 255), 8).execute(directionService.getDirectionsUrl(_model.getUserLocation(), nearUserStation.getLatLng()));
				new GetDirectionJson(_activity, Color.argb(120, 255, 0, 0), 10).execute(directionService.getDirectionsUrl(nearUserStation.getLatLng(), nearTargetLocationStation.getLatLng()));
				new GetDirectionJson(_activity, Color.argb(120, 0, 0, 255), 8).execute(directionService.getDirectionsUrl(nearTargetLocationStation.getLatLng(), marker.getPosition()));
				//marker.remove();
				nevigationDrawer.setVisibility(View.VISIBLE);
		    }
		}; 
		this.destinationBtn.setOnTouchListener(destinationButtonListener);

		
		//評論
		this.commentButtonListener = new OnInfoWindowElemTouchListener(commentBtn) 
		{
			@Override
			 protected void onClickConfirmed(View v, Marker marker) {
				for(PlaceMarker placeMarker: _activity.getPlaceMarkerList()){
					if(placeMarker.getMarker().equals(marker)){
			    		_model.setLatLngForStreetView(placeMarker);
			    		break;
					}
				}
				_activity.streetView();
			}
		    
		}; 
		this.commentBtn.setOnTouchListener(commentButtonListener);
		

		//詳細資料
		this.moreDetailButtonListener = new OnInfoWindowElemTouchListener(moreDetailBtn){
				@Override
				 protected void onClickConfirmed(View v, Marker marker) {
					Intent intent = new Intent();
					intent.setClass(_myMarkerView.getContext(),CommentActivity.class);
					intent.putExtra("Reference",reference); 
					
					_myMarkerView.getContext().startActivity(intent);
				}
			    
			}; 
		this.moreDetailBtn.setOnTouchListener(moreDetailButtonListener);
	}

    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dp * scale + 0.5f);
    }
    
	public View getInfoWindow(Marker marker) {
	    return null;
	}
	
	public View getInfoContents(Marker marker) {

		// Setting up the infoWindow with current's marker info
        nameText.setText(marker.getTitle());
        addressText.setText(marker.getSnippet());
        destinationButtonListener.setMarker(marker);
        moreDetailButtonListener.setMarker(marker);
        commentButtonListener.setMarker(marker);

        if(marker.getTitle() == null)
        {
        	return null;
        }
		Place place = null;
		PlaceMarker placemarker = null;
		
		for(PlaceMarker placeMarker: _activity.getPlaceMarkerList()){
			if(placeMarker.getMarker().equals(marker)){
				place  = placeMarker.getPlace();
				placemarker = placeMarker;
			}
		}

		nameText.setText("");
		addressText.setText("");
		distanceText.setText("");
        phoneText.setText("");
        otherText.setText("");

		if(placemarker != null){
			otherText.setText(placemarker.getSnippet());
		}
        // Setting the latitude
        try {
        	if(place != null)
        	{
        		nameText.setText(place.getName());
        		if(placemarker.getType() != R.string.MRT && placemarker.getType() != R.string.BUS && placemarker.getType() != R.string.UBIKE){
        			addressText.setText("地址:" + place.getDetail().getString("formatted_address"));
        			phoneText.setText("電話:"+ place.getDetail().getString("formatted_phone_number"));
        			reference = place.getDetail().getString("reference");
        			moreDetailBtn.setVisibility(View.VISIBLE);
        		}else
        		{
        			moreDetailBtn.setVisibility(View.INVISIBLE);
        		}
        	}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
        // We must call this to set the current marker and infoWindow references
        // to the MapWrapperLayout
        mapWrapperLayout.setMarkerWithInfoWindow(marker, infoWindow);
        return infoWindow;
	}
	
	
	
	private void render(Marker marker, View view) {
	   // Add the code to set the required values 
	   // for each element in your custominfowindow layout file
	}
}