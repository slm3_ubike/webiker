package edu.ntut.ubike;
import com.google.android.gms.maps.model.Marker;

import edu.ntut.place.Place;

public class PlaceMarker {
	private int _type;
	private String _title;
	private String _snippet;
	private Marker _marker;
	private Place _place;
	
	public PlaceMarker()
	{
		_type = 0;
		_title = "";
		_snippet = "";
		_marker = null;
	}
	
	public PlaceMarker(int type, String title, String snippet, Marker marker)
	{
		_type = type;
		_title = title;
		_snippet = snippet;
		_marker = marker;
	}
	
	public void remove()
	{
		_type = 0;
		_title = "";
		_snippet = "";
		_marker.remove();
	}
	
	public int getType()
	{
		return _type;
	}
	
	public void setType(int type)
	{
		_type = type;
	}
	
	public String getTitle()
	{
		return _title;
	}
	
	public void setTitle(String title)
	{
		_title = title;
	}
	
	public String getSnippet()
	{
		return _snippet;
	}
	
	public void setSnippet(String snippet)
	{
		_snippet = snippet;
	}
	
	public Marker getMarker()
	{
		return _marker;
	}
	
	public void setMarker(Marker marker)
	{
		_marker = marker;
	}

	public Place getPlace()
	{
		return _place;
	}
	
	public void setPlace(Place place)
	{
		_place = place;
	}
}
