package edu.ntut.ubike;

import edu.ntut.place.GetPlaces;
import android.app.ProgressDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class PlaceBtnClickListener implements Button.OnClickListener{
	private Model _model;
	private MainActivity _activity;
	private boolean _isClick;
	private int _type;
	private String _searchKeyword;
	
	public PlaceBtnClickListener(Model model, MainActivity activity,int type){
		_model = model;
		_activity = activity;
		_isClick = false;
		_type = type;
		
		switch(_type)
		{
			case R.string.EAT:
				_searchKeyword = "food";
				break;
			
			case R.string.BUS:
				_searchKeyword = "bus_station";
				break;
			
			case R.string.SEVEN:
				_searchKeyword = "convenience_store";
				break;
				
			case R.string.ART:
				_searchKeyword = "art_gallery";
				break;
				
			case R.string.MRT:
				_searchKeyword = "train_station";
				break;
		}
	}
	
	@Override
	public void onClick(View arg0) {
		if(_isClick){
			_activity.dispearMapMarker(_type);
		}
		else{
			GetPlaces getPlaces = new GetPlaces(_searchKeyword, _model, _activity, _type);
			((ImageButton)arg0).setEnabled(false);
			getPlaces.execute();
		}
		_isClick = !_isClick;
	}
}
