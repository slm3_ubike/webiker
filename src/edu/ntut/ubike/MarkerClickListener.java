package edu.ntut.ubike;

import android.graphics.Color;

import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.Marker;

import edu.ntut.JsonHelper.Station;
import edu.ntut.googleDirection.DirectionService;
import edu.ntut.googleDirection.GetDirectionJson;
import edu.ntut.googleDirection.GetTimeAndDistanceJson;

public class MarkerClickListener implements OnMarkerClickListener{

	private MainActivity _activity;
	private Model _model;
	
	public MarkerClickListener(MainActivity activity, Model model) {
		_activity = activity;
		_model = model;
	}
			
	@Override
	public boolean onMarkerClick(Marker marker) {
		
		//設定終點的marker click事件
		if(marker.getTitle() == null){
			_activity.clearRoute();
			Station nearUserStation = _model.getNearestStation(_model.getUserLocation());
			Station nearTargetLocationStation = _model.getNearestStation(marker.getPosition());
			DirectionService directionService = new DirectionService();
			new GetDirectionJson(_activity, Color.argb(120, 0, 0, 255), 8).execute(directionService.getDirectionsUrl(_model.getUserLocation(), nearUserStation.getLatLng()));
			new GetDirectionJson(_activity, Color.argb(120, 255, 0, 0), 10).execute(directionService.getDirectionsUrl(nearUserStation.getLatLng(), nearTargetLocationStation.getLatLng()));
			new GetDirectionJson(_activity, Color.argb(120, 0, 0, 255), 8).execute(directionService.getDirectionsUrl(nearTargetLocationStation.getLatLng(), marker.getPosition()));
			marker.remove();
		}
		else{
			//標記從現在位置，到marker的時間
//			for(PlaceMarker placeMarker: _activity.getPlaceMarkerList()){
//				if(placeMarker.getMarker().equals(marker)){
//					DirectionService directionService = new DirectionService();
//		    		new GetTimeAndDistanceJson(placeMarker).execute(directionService.getDirectionsUrl(_model.getUserLocation(), marker.getPosition()));
//		    		_model.setLatLngForStreetView(placeMarker);
//		    		break;
//				}
//			}
		}

		return false;
	}
}
