package edu.ntut.googleDirection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import edu.ntut.ubike.MainActivity;
import android.os.AsyncTask;

public class GetDirectionJson extends AsyncTask<String, Void, String> {  
	private MainActivity _activity;
	private int _color;
	private int _width;
	
	public GetDirectionJson(MainActivity activity, int color, int width){
		_activity = activity;
		_color = color;
		_width = width;
	}
	
    @Override  
    protected String doInBackground(String... url) {  
        // For storing data from web service  
        String data = "";  
        try {  
            // Fetching the data from web service  
            data = downloadUrl(url[0]);  
        } catch (Exception e) {  
        }  
        return data;  
    }  
  
    // Executes in UI thread, after the execution of  
    // doInBackground()  
    @Override  
    protected void onPostExecute(String result) {  
        super.onPostExecute(result);  
  
        GetDirection parserTask = new GetDirection(_activity, _color, _width);  
  
        // Invokes the thread for parsing the JSON data  
        parserTask.execute(result);  
    }  
    
    public String downloadUrl(String strUrl) throws IOException {  
	    String data = "";  
	    InputStream iStream = null;  
	    HttpURLConnection urlConnection = null;  
	    try {  
	        URL url = new URL(strUrl);  
	        // Creating an http connection to communicate with url  
	        urlConnection = (HttpURLConnection) url.openConnection();  
	        // Connecting to url  
	        urlConnection.connect();  
	        // Reading data from url  
	        iStream = urlConnection.getInputStream();  
	        BufferedReader br = new BufferedReader(new InputStreamReader(iStream)); 
	        StringBuffer sb = new StringBuffer();  
	        String line = "";  
	        while ((line = br.readLine()) != null) {  
	            sb.append(line);  
	        }  
	        data = sb.toString();  
	        br.close();  
	    } catch (Exception e) {  
	    	
	    } finally {  
	        iStream.close();  
	        urlConnection.disconnect();  
	    }
	    return data;  
	}  
}  
