package edu.ntut.googleDirection;

import com.google.android.gms.maps.model.LatLng;

public class DirectionService {
	public String getDirectionsUrl(LatLng origin, LatLng dest) {  
	    // Origin of route  
	    String str_origin = "origin=" + origin.latitude + "," + origin.longitude;  
	    // Destination of route  
	    String str_dest = "destination=" + dest.latitude + "," + dest.longitude;  
	    // Sensor enabled  
	    String sensor = "sensor=false";  
	    // Travelling Mode  
	    String mode = "mode=walking";
	    String lang = "language=zh-TW";
	    // Building the parameters to the web service  
	    String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode+"&" + lang;  
	    // Output format  
	    String output = "json";
	    // Building the url to the web service  
	    String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;  
	    return url;  
	}
}
