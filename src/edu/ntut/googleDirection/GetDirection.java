package edu.ntut.googleDirection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import edu.ntut.ubike.MainActivity;

public class GetDirection extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
	private ProgressDialog _dialog;
	private MainActivity _activity;
	private int _color;
	private int _width;
	public GetDirection(MainActivity activity, int color, int width){
		_activity = activity;
		_color = color;
		_width = width;
		_dialog = new ProgressDialog(activity);
	}
	
	@Override  
	protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {  
		JSONObject jObject;  
		List<List<HashMap<String, String>>> routes = null;  
		
		try {  
		    jObject = new JSONObject(jsonData[0]);  
		    DirectionJSONParser parser = new DirectionJSONParser();  
		
		    // Starts parsing data  
		    routes = parser.parse(jObject);
		} catch (Exception e) {  
		    e.printStackTrace();  
		}  
		return routes;  
	}  
	
	@Override
	protected void onPreExecute() {
		this._dialog.setTitle("資料讀取中");
		this._dialog.setMessage("請稍候");
        this._dialog.show();
		super.onPreExecute();
	}
  
	@Override  
	protected void onPostExecute(List<List<HashMap<String, String>>> result) {  
		ArrayList<LatLng> points = null;  
		PolylineOptions lineOptions = null;
		ArrayList<String> html_str_array = null;
		
		// Traversing through all the routes  
		for (int i = 0; i < result.size(); i++) {  
			
			if (i == 0) {
			    points = new ArrayList<LatLng>();  
			    lineOptions = new PolylineOptions();  
			    // Fetching i-th route  
			    List<HashMap<String, String>> path = result.get(i);  
			    html_str_array = new ArrayList<String>();
			    
			    // Fetching all the points in i-th route  
			    for (int j = 0; j < path.size(); j++) {  
			        HashMap<String, String> point = path.get(j); 
			        
			        if(point.get("html_str") != null)
			        {
			            //System.out.println("****   " + html_list.get(k));
			            String html_str = point.get("html_str");
			            System.out.println("----   " + html_str);
			            html_str_array.add(html_str);
			        }
			        else if(point.get("lat") != null)
			        {
			        	double lat = Double.parseDouble(point.get("lat"));  
			        	double lng = Double.parseDouble(point.get("lng"));  
			        	LatLng position = new LatLng(lat, lng);  
			
			        	points.add(position);  
			        }
			    }
			}
		
		    // Adding all the points in the route to LineOptions  
		    lineOptions.addAll(points);  
		    lineOptions.width(_width);
		    // Changing the color polyline according to the mode  
		    lineOptions.color(_color);  
		}  
		// Drawing polyline in the Google Map for the i-th route  
		_activity.addRoute(lineOptions);
		_activity.addText(html_str_array);
		
		if (_dialog.isShowing())
            _dialog.dismiss();
	}  
}  
