package edu.ntut.googleDirection;

import java.util.HashMap;

import org.json.JSONObject;


import edu.ntut.ubike.PlaceMarker;

import android.os.AsyncTask;

public class GetTimeAndDistance extends AsyncTask<String, Integer, HashMap<String, String>>{

	private PlaceMarker _placeMarker;
	
	public GetTimeAndDistance(PlaceMarker placeMarker){
		_placeMarker = placeMarker;
	}
	
	@Override  
	protected HashMap<String, String> doInBackground(String... jsonData) {  
		JSONObject jObject;  
		HashMap<String, String> timeAndDistance = null;  
		
		try {  
		    jObject = new JSONObject(jsonData[0]);  
		    DirectionJSONParser parser = new DirectionJSONParser();  
		
		    // Starts parsing data  
		    timeAndDistance = parser.getTimeAndDistanceParse(jObject);
		} catch (Exception e) {  
		    e.printStackTrace();  
		}  
		return timeAndDistance;  
	}  
  
	@Override  
	protected void onPostExecute(HashMap<String, String> result) {
		_placeMarker.getMarker().setSnippet(_placeMarker.getSnippet() + " �Z��: " + result.get("distance") + " �ɶ�: " + result.get("duration"));
		_placeMarker.getMarker().showInfoWindow();
	}  
}
